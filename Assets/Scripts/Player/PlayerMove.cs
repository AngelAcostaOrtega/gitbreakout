﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [SerializeField][Header("Movimiento")]
    private Vector2 Direction;
    [SerializeField][Range(0, 5)]private int speed = 0;

    void Update()
    {
        /*
         * La entrada de movimiento hacia la derecha, se cumple si pulsamos la tecla 'D'
         * o si pulsamos la pantalla y el cursor o dedo está a la derecha del centro.
         * Direccion será igual a (1, 0);
         */
        if ( Input.GetKey(KeyCode.D) || (Input.GetMouseButton(0) && Camera.main.ScreenToWorldPoint(Input.mousePosition).x > 0.1f) )
        {
            Direction = Vector2.right;
        }
        /*
         * Al igual que el anterior pero en este caso hacia la izquierda, se cumple si pulsamos la tecla 'A'
         * o si pulsamos la pantalla y el cursor o dedo está a la izquierda del centro.
         * Direccion será igual a (-1, 0);
         */
        if (Input.GetKey(KeyCode.A) || (Input.GetMouseButton(0) && Camera.main.ScreenToWorldPoint(Input.mousePosition).x < 0.1f) )
        {
            Direction = Vector2.left;
        }
        /*
         * Comprueba si hemos levantado el dedo o cursor o, en PC, hemos dejado de pulsar las teclas
         * Esto evita que en el caso del cursor o dedo se quede la plataforma moviendose en la misma direccion hasta
         * hacer colision.
         */
        if (Input.GetMouseButtonUp(0) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D)) Direction = Vector2.zero;
        /*
         * Este if llama a los dos métodos tanto de movimiento como de colision en caso de que la direccion actual
         * sea distinta de (0, 0);
         */
        if (Direction != Vector2.zero)
        {
            Collisions();
            Move();
        }
    }

    //Método que comprueba si, en la dirección actual, hay algun obstáculo (No me gusta usar el OnCollisionEnter para los "personajes").
    private void Collisions()
    {
        Vector2 col = new Vector2(this.transform.position.x + ( (this.GetComponent<BoxCollider2D>().bounds.extents.x + 0.1f) * Direction.x), this.transform.position.y );
        if(Physics2D.Raycast( col, Direction, 0.1f ) )
        {
            Direction = Vector2.zero;
        }
    }


    //Método que mueve la pala en la dirección actual.
    private void Move()
    {
        this.transform.Translate(Direction * speed * Time.deltaTime);
    }
}
