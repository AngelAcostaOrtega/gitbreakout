﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMng : MonoBehaviour
{
    public void refresh()
    {
        BoxCollider2D[] hijos;
        hijos = this.GetComponentsInChildren<BoxCollider2D>();
        if(hijos.Length <= 1)
        {
            GameObject.Find("BackGrounds").GetComponent<backgroundMove>().moveDown();

            GameObject.FindGameObjectWithTag("Ball").GetComponent<BallPhysics>().explode();
        }
    }
}
