﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private bool inGame;
    void Start()
    {
        inGame = false;
        GameObject.Find("GUI/Canvas/fireBtn").SetActive(true);
    }

    void Update()
    {
        /*
         * Este if eyectará la pelota contra la pala si se pulsa la tecla 'S'
         * o si se pulsa el botón eyectar que llamará al mismo método.
         */
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Eyectar();
        }
        /*
         * Esta parte del código hará que la pelota "persiga" a la pala en caso de estár fuera de juego.
         */
        if (!inGame)
        {
            this.transform.position = new Vector2(
                GameObject.Find("Player").transform.position.x,
                GameObject.Find("Player").transform.position.y + (GameObject.Find("Player").GetComponent<BoxCollider2D>().bounds.extents.y * 3));
        }
    }

    //Método que lanza la bola hacia abajo en caso de estar fuera de juego para que rebote en la pala.
    public void Eyectar()
    {
        if (!inGame)
        {
            GameObject.Find("GUI/Canvas/fireBtn").SetActive(false);
            inGame = true;
            this.GetComponent<Rigidbody2D>().AddForce((Vector2.down * Time.deltaTime) / 1f);
        }
    }
}
