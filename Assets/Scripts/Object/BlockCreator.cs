﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCreator : MonoBehaviour
{
    /*
     * Veamos, como explico esto Xd.
     * Lo primero aquí tenemos las variables que van a ser necesarias, como por ejemplo:
     * 
     *      - Dos int, que van a ser la cantidad de columnas y filas que tenga la partida respectivamente.
     *        Estas están limitadas a una cantidad que nos permitirá poner el máximo de bloques sin que se salgan
     *        de la pantalla o que tenga más filas que vidas los bloques.
     *        
     *      - El array de arrays que va a guardas los valores de los GameObject por si acaso me hace falta
     *        obtenerlos más tarde y necesito ponerlo en publico y porque era el método más simple de acceder
     *        al anterior para colocar el nuevo.
     *        
     *      - El GameObject del objeto base que vamos a usar es decir, el bloque.
     *      
     *      - Y por ultimo un Vector2 que vamos a usar para que quede más bonito en instanciamiento, ya que
     *        si no, quedaría un código más extraño e incomprensible.
     *        
     */
    [SerializeField, Header("Filas y Columnas")][Range(0,3)]
    private int col;
    [SerializeField][Range(0, 4)]
    private int row;
    
    private GameObject[,] tabla;

    [SerializeField, Header("Objeto instaciable")]
    private GameObject block;

    private Vector2 pos;

    private void Start()
    {
        /*
         * Lo primero que he hecho es asignarle un valor a las variables que en verdad se les asigna el valor
         * desde la consola porque sino salen LogAdverts que no es que hagan fallar al juego pero ponen bastante
         * nervioso a uno.
         */
        if (col == 0) col = 1;
        if (row == 0) row = 1;
        if (block == null) block = null;

        //Llamada al método con la creacion de bloques
        this.create(col, row);
    }
    public void create(int col, int row)
    {
        /*
         * A continuacion he definido la tabla 2D de GameObjects con los valores deseados de columnas y filas.
         */
        this.col = col;
        this.row = row;
        tabla = new GameObject[row,col];

        /*
         * Y aqui empieza lo bueno, con un for doble para arrays 2D de toda la vida vamos recorriendo uno a uno
         * todos los espacios en la tabla.
         */
        for (int i = 0; i < row; i++)
        {
            for(int j = 0; j < col; j++)
            {
                //En caso de que sea el primero lo que hacemos es instanciar un objeto en la propia posicion.
                if (i == 0 && j == 0)
                {
                    //Tomamos la posicion.
                    pos = this.transform.position;

                    this.crearBloque(i, j);
                }
                else
                {
                    /*
                     * Ahora es basicamente lo mismo excepto por la posición.
                     * En caso de no ser el primer registro de su fila, tomará la posicion del anterior añadiendole el triple de su radio
                     * lateral para quedar separado.
                     * En caso contrario, si es el primero de su fila, tomará la posicion del que tiene justo encima multiplicando su
                     * radio vertical por 3 para quedar separado de nuevo.
                     */
                    if (j != 0)
                    {
                        pos.x = tabla[i, j - 1].transform.position.x + (tabla[i, j - 1].GetComponent<BoxCollider2D>().bounds.extents.x * 3);
                        pos.y = tabla[i, j - 1].transform.position.y;

                        this.crearBloque(i, j);
                    }
                    else
                    {
                        pos.x = tabla[i - 1, j].transform.position.x;
                        pos.y = tabla[i - 1, j].transform.position.y - (tabla[i - 1, j].GetComponent<BoxCollider2D>().bounds.extents.y * 3);

                        this.crearBloque(i, j);
                    }
                }
            }
        }
        Destroy(this.gameObject.GetComponent<BoxCollider2D>(), 0.2f);
    }

    private void crearBloque(int i, int j)
    {
        //Instanciamos el objeto en la posicion y angulo deseado haciendo que su padre sea el mismo para todos los bloques.
        tabla[i, j] = (GameObject)Instantiate(block, pos, Quaternion.identity, GameObject.Find("BlocksFather").transform);
        //Le cambiamos la vida para que tenga una vida acorde a su altura en la tabla
        tabla[i, j].GetComponent<BlockLifes>().Lifes = row - i;
        //Se le asigna el valor de puntuacion necesario en funcion a su linea.
        tabla[i, j].GetComponent<BlockLifes>().score = (int)((row - i) * 10 * (1f + (0.25f * ((row - i) - 1))));
        //Y finalmente llamamos al metodo que cambia su sprite en funcion de la vida que tenga.
        tabla[i, j].GetComponent<BlockLifes>().changeSprite();
    }
}
