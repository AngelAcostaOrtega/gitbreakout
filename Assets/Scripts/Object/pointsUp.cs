﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pointsUp : MonoBehaviour
{
    private Vector2 Final;
    private float time = 1f, Delta = 0f;

    private void Start()
    {
        Final = new Vector2(this.transform.position.x, this.transform.position.y + 1);
    }
    void Update()
    {
        this.transform.position = Vector2.Lerp(this.transform.position, Final, 2 * Time.deltaTime);
        Delta += Time.deltaTime;
        if (Delta >= time)
        {
            this.gameObject.SetActive(false);
            Destroy(this.gameObject, 0.1f);
        }
    }
}
