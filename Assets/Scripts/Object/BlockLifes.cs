﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockLifes : MonoBehaviour
{
    /*
     * Las variables que va a tener este script son:
     *      - Un Array de Sprites para las vidas del bloque
     *      - Y sus propias vidas, limitadas del 0 al 4.
     */
    [SerializeField][Header("Sprites")]
    public Sprite[] sprites = new Sprite[4];

    [SerializeField][Header("Vidas")][Range(0, 4)]public int Lifes;

    [HideInInspector] public int score;

    private AudioSource[] speakers;

    [SerializeField][Header("Prefab Puntos")]
    private GameObject point = null;

    /*
     * Un constructor en caso de que fuera necesario, al final resulta que no lo fue
     * pero podría haberlo sido o podría serlo en el futuro.
     */
    public BlockLifes(int hp)
    {
        Lifes = hp;
    }


    private void Start()
    {
        speakers = this.GetComponents<AudioSource>();

        if (score == 0) score = 10;

        if (Lifes <= 0)
        {
            for (int n = 0; n <= 3; n++)
            {
                if (this.GetComponent<SpriteRenderer>().sprite.name.Equals(sprites[n].name))
                {
                    Lifes = n + 1;
                    break;
                }
            }
        }
        else
        {
            changeSprite();
        }
    }

    /*
     * A partir de aquí controlaremos las colisiones, algo que va a ser facil porque lo unico que puede chocar con los bloques
     * es la pelota.
     */

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Ball"))
        {
            /*
             * Siempre que la bola choque con un bloque esta le quitará uno de vida.
             * Si su vida es mayor de cero despues de restarsela, se le cambiará el sprite llamando al método correspondiente.
             * Si en caso contrario su vida ha alcanzado el 0, este se desactivará para no ocupar tanto y hacer mas simple
             * su posterior destruccion (un pequeño truco para cuando hay muchos objetos y no queremos "petar" tanto el PC).
             */
            Lifes--;
            if (Lifes > 0)
            {
                speakers[0].Play();

                this.sumarPuntos(5);

                changeSprite();
            }
            else
            {
                speakers[1].Play();

                this.sumarPuntos(this.score);

                Destroy(this.gameObject.GetComponent<BoxCollider2D>());
                Destroy(this.gameObject.GetComponent<SpriteRenderer>());
                Destroy(this.gameObject, speakers[1].clip.length);
                this.gameObject.GetComponentInParent<BlockMng>().refresh();
            }
        }
    }
    /*
     * Un método sencillo que cambia el sprite del objeto en función de su vida.
     */
    public void changeSprite()
    {
        this.GetComponent<SpriteRenderer>().sprite = sprites[Lifes - 1];
    }

    private void sumarPuntos(int n)
    {
        GameObject ob = GameObject.Find("GUI/Canvas/MenuInGame/menu/Puntuacion");
        ob.GetComponent<scoreUp>().score += n;
        ob.GetComponent<scoreUp>().scoreChange();

        GameObject p = point;
        p.GetComponent<TMPro.TextMeshPro>().text = n.ToString();
        Instantiate(p, this.transform.position, Quaternion.identity);
    }
}
