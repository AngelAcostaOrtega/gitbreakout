﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPhysics : MonoBehaviour
{
    private AudioSource[] speakers;

    private void Start()
    {
        speakers = this.GetComponents<AudioSource>();
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        /*
         * EN CASO DE CHOCAR CONTRA EL LÍMITE BAJO, O PUNTO, EJECUTARA UNA SERIE DE EVENTOS QUE VAN DESDE
         * LA DESTRUCCIÓN Y EXPLOSION DE PARTÍCULAS DE LA MISMA BOLA, HASTA QUITARNOS UN PUNTO DE VIDA
         * Y ACTIVAR EL INSTANCIADOR DE PELOTAS HACIENDO ASÍ UNA NUEVA PELOTA.
         */
        if (col.gameObject.CompareTag("Punto"))
        {
            /*
             * Llamada al método que se encarga de la destrucción de la bola y los componentes necesarios para que parezca que "explota"y que deja de moverse.
             * Así como la activacion del hijo con el sistema de partículas.
             */
            explode();

            /*
             * Llamará al método del Game manager para hacer que se nos reste una vida.
             */

            Camera.main.GetComponent<GameMotor>().restarVida();
        }

        /*
         * CALCULAMOS LOS LIMITES DE LA PALA PARA PODER HACER EL EFECTO DE GOLPEAR DE LADO CON EL CUARTO DERECHO
         * Y EL CUARTO IZQUIERDO DE LA PALA. UNA VEZ CALCULADO COMPROBAMOS QUE LA VELOCIDAD DE LA PELOTA DESDE SU CENTRO SEA
         * HACIA EL LADO DE LA PALA Y SI NO ES ASÍ PODER CONVERTIRLO HACIA ESE LADO. EJEMPLO: GOLPEAMOS CON EL LADO DERECHO PERO
         * LA VELOCIDAD ES HACIA LA IZQUIERDA, ENTONCES LA FUERZA A APLICAR SERÍA EL DOBLE DE LO NORMAL PARA CONTRARRESTAR DICHA
         * VELOCIDAD Y DEVOLVER LA PELOTA HACIA LA DERECHA.
         * Y EN CASO DE QUE LA PELOTA ESTÉ RECTA ES DECIR QUE SU VELOCIDAD NO SEA NI HACIA LA IZQUIERDA NI MAYOR O IGUAL QUE LA QUE
         * APLICAMOS HACIA LA DERECHA, SE MUEVA HACIA LA DERECHA, DE ESTA FORMA EVITAMOS SEGUIR AÑADIENDO MAS Y MAS FUERZA EN CASO
         * DE QUE LA PELOTA GOLPEE CONSTANTEMENTE EN UN MISMO LADO.
         */

        //En caso de golpear la pala del jugador
        else if (col.gameObject.CompareTag("Player"))
        {
            speakers[1].Play();
            //En caso de que golpee el cuarto derecho de la pala
            if ( this.transform.position.x - col.transform.position.x > col.gameObject.GetComponent<BoxCollider2D>().bounds.extents.x / 2 )
            {
                //En caso de que la velocidad en el punto central de la pelota sea hacia la izquierda
                if ( ( this.GetComponent<Rigidbody2D>().GetRelativePointVelocity(Vector2.zero).x <= ((Vector2.left.x * Time.deltaTime) / 2) ) )
                {
                    this.GetComponent<Rigidbody2D>().AddRelativeForce((Vector2.right * Time.deltaTime) );
                }
                //En caso de que la velocidad en el punto central de la pelota no sea hacia la derecha
                else if ( !( this.GetComponent<Rigidbody2D>().GetRelativePointVelocity(Vector2.zero).x >= ((Vector2.right.x * Time.deltaTime)/2) ) )
                {
                    this.GetComponent<Rigidbody2D>().AddRelativeForce((Vector2.right * Time.deltaTime) / 2);
                }
            }
            //En caso de que golpee el cuarto izquierdo de la pala
            else if (this.transform.position.x - col.transform.position.x < -col.gameObject.GetComponent<BoxCollider2D>().bounds.extents.x / 2 )
            {
                //En caso de que la velocidad en el punto central de la pelota sea hacia la derecha
                if ( ( this.GetComponent<Rigidbody2D>().GetRelativePointVelocity(Vector2.zero).x >= ((Vector2.right.x * Time.deltaTime) / 2) ) )
                {
                    this.GetComponent<Rigidbody2D>().AddRelativeForce((Vector2.left * Time.deltaTime) );
                }
                //En caso de que la velocidad en el punto central de la pelota no sea hacia la izquierda
                else if ( !( this.GetComponent<Rigidbody2D>().GetRelativePointVelocity(Vector2.zero).x <= ((Vector2.left.x * Time.deltaTime) / 2) ) )
                {
                    this.GetComponent<Rigidbody2D>().AddRelativeForce((Vector2.left * Time.deltaTime) / 2);
                }
            }
        }
        else if(!col.gameObject.CompareTag("Blocks"))
        {
            speakers[0].Play();
        }
    }

    public void explode()
    {
        speakers[2].Play();
        Destroy(this.GetComponent<SpriteRenderer>());
        this.transform.GetChild(0).gameObject.SetActive(true);
        Destroy(this.GetComponent<Rigidbody2D>(), 0.01f);
        Destroy(this.gameObject, speakers[2].clip.length);
    }
}
