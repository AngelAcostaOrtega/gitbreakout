﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backgroundMove : MonoBehaviour
{
    [HideInInspector]public Vector2 Posicion;
    private bool moving;
    private float high;
    private int lvl;

    private void Start()
    {
        lvl = 1;
        high = this.transform.GetChild(0).GetComponent<BoxCollider2D>().bounds.size.y;
        Destroy(this.transform.GetChild(0).GetComponent<BoxCollider2D>(), 0.01f);
    }

    void Update()
    {
        if (moving)
        {
            this.transform.position = Vector2.Lerp(this.transform.position, Posicion, 2f * Time.deltaTime);

            if (this.transform.position.y <= Posicion.y+0.01f)
            {
                moving = false;

                GameObject.Find("BlocksFather/Block").TryGetComponent(out BlockCreator bc);
                if (lvl < 4) bc.create(3, lvl);
                else bc.create(3, 4);
                Camera.main.GetComponent<GameMotor>().newLvl(lvl);
            }
        }
    }

    public void moveDown()
    {
        Posicion = new Vector2(this.transform.position.x, this.transform.position.y - high);
        lvl++;
        if (lvl <= 5) moving = true;
        else Camera.main.GetComponent<GameMotor>().finish(); ;
    }
}
