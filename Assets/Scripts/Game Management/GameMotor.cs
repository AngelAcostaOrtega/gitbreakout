﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMotor : MonoBehaviour
{
    [HideInInspector]
    public int Lifes;

    [SerializeField][Header("Lifes UI father")]
    private GameObject LifeMng;

    [SerializeField][Header("Ball prefab")]
    private GameObject Ball;

    [SerializeField][Header("Music")]
    private float startVolume = 0.2f;
    [SerializeField]
    private float finalVolume = 0.8f;
    [SerializeField][Range(0, 1)]
    private float volumeUpSpeed = 0.5f;
    private bool volumen = false;
    
    void Start()
    {
        Lifes = 3;
        //Estos valores no necestian asignacion pero pongo estas lineas porque no quiero que me salga el LogAdvert en consola
        if (LifeMng == null) LifeMng = null;
        if (Ball == null) Ball = null;

        mute();
        music();
        startMusic();
        helpPad();
    }
    private void Update()
    {
        if (volumen)
        {
            this.TryGetComponent(out AudioSource n);
            if (n.volume < finalVolume)
            {
                n.volume += volumeUpSpeed * Time.deltaTime;
            }
            else volumen = false;

        }
    }

    public void newLvl(int lvl)
    {
        GameObject.Find("GUI/Canvas/MenuInGame").SetActive(false);
        GameObject.Find("GUI/Canvas/Level").SetActive(true);
        GameObject.Find("GUI/Canvas/Level").GetComponent<LevelGUI>().changeLevel(lvl);
    }
    public void startLvl()
    {
        if(GameObject.FindGameObjectWithTag("Ball") == null)
        {
            Instantiate(Ball);
        }
    }

    public void restarVida()
    {
        Lifes--;

        if (LifeMng.GetComponent<UpdateLifes>().updateLifes(Lifes))
        {
            Instantiate(Ball);
        }
        else
        {
            lose();
        }
    }

    public void lose()
    {
        this.WinWin("Wasted");
    }

    public void finish()
    {
        this.WinWin("Victory");
    }

    public int record()
    {
        if (GameObject.Find("GUI/Canvas/MenuInGame/menu/Puntuacion").GetComponent<scoreUp>().score > PlayerPrefs.GetInt("1Record_Score"))
        {
            int r = GameObject.Find("GUI/Canvas/MenuInGame/menu/Puntuacion").GetComponent<scoreUp>().score;
            PlayerPrefs.SetString("1Record_Player", PlayerPrefs.GetString("Player_name", "RPly"));
            PlayerPrefs.SetInt("1Record_Score", r);
            return r;
        }
        else
        {
            return PlayerPrefs.GetInt("1Record_Score");
        }
    }

    public void WinWin(string n)
    {
        GameObject.Find("GUI/Canvas/" + n + "/Panel/pointsScore").GetComponent<TMPro.TextMeshProUGUI>().text =
                (GameObject.Find("GUI/Canvas/MenuInGame/menu/Puntuacion").GetComponent<scoreUp>().score).ToString();

        GameObject.Find("GUI/Canvas/" + n + "/Panel/recordScore").GetComponent<TMPro.TextMeshProUGUI>().text = (this.record()).ToString();

        GameObject.Find("GUI/Canvas/" + n).SetActive(true);

        Destroy( GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMove>() );
        if (!n.Equals("Victory")) Time.timeScale = 0.0f;
    }

    public void mute()
    {
        this.TryGetComponent(out AudioListener n);
        n.enabled = (PlayerPrefs.GetInt("Mute") == 0 ? true : false);
    }
    public void changeMute()
    {
        PlayerPrefs.SetInt("Mute", ( PlayerPrefs.GetInt("Mute") != 0 ? 0:1 ));
        mute();
    }
    
    public void music()
    {
        this.TryGetComponent(out AudioSource n);
        n.mute = (PlayerPrefs.GetInt("Music") != 0 ? true:false);
    }
    public void changeMusic()
    {
        PlayerPrefs.SetInt("Music", (PlayerPrefs.GetInt("Music") != 0 ? 0:1));
        music();
    }

    public void startMusic()
    {
        this.TryGetComponent(out AudioSource n);
        n.Play();
        n.volume = startVolume;
        volumen = true;
    }

    public void helpPad()
    {
        GameObject n = GameObject.Find("GUI/Canvas/MenuInGame/helpBtns");
        n.SetActive( ( PlayerPrefs.GetInt("helpPad") == 0 ? true : false ) );
    }
    public void changeHelp()
    {
        PlayerPrefs.SetInt("helpPad", (PlayerPrefs.GetInt("helpPad") != 0 ? 0 : 1));
        helpPad();
    }
}
