﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PosicionamientoLimites : MonoBehaviour
{
    /*
     * ESTE SCRIPT ESTÁ EHCHO PARA AUTOCOLOCAR LOS LÍMITES JUSTO EN LOS BORDES DE LA PANTALLA.
     * TIENE UN ARRAY DE 5 PARA PODER COGER LOS TRANSFORM DE LAS CUATRO PAREDES SALTANDOSE EL
     * DE LA POSICIÓN 0 QUE SERÍA EL TRANSFORM DEL OBJETO "LIMITS" QUE ENGLOBA A LOS OTROS
     * CUATRO, VAMOS EL TRANSFORM DEL PADRE VAYA.
     */
    private Transform[] hijos = new Transform[5];

    void Start()
    {
        hijos = this.transform.GetComponentsInChildren<Transform>();

        //Bajo
        hijos[1].position = Camera.main.ScreenToWorldPoint( (Vector3)(new Vector2(Camera.main.scaledPixelWidth / 2f, 0f)) );
        //Alto (Complicado de explicar)
        float x = Camera.main.scaledPixelWidth / 2f;
        float y = Camera.main.WorldToScreenPoint(
                (Vector3)new Vector2(
                    0f,
                    (GameObject.Find("BlocksFather").transform.GetChild(0).transform.position.y + GameObject.Find("BlocksFather").transform.GetChild(0).GetComponent<BoxCollider2D>().bounds.extents.y + 0.1f)
                )
            ).y;
        hijos[2].position = Camera.main.ScreenToWorldPoint( (Vector3)( new Vector2(x, y)));
        //Derecho
        hijos[3].position = Camera.main.ScreenToWorldPoint((Vector3)(new Vector2(Camera.main.scaledPixelWidth, Camera.main.scaledPixelHeight / 2)));
        //Izquierdo
        hijos[4].position = Camera.main.ScreenToWorldPoint((Vector3)(new Vector2(0, Camera.main.scaledPixelHeight / 2)));

        //Debug.Log("Hijos actuales: " + hijos[1].name + hijos[2].name + hijos[3].name + hijos[4].name);
    }
}
