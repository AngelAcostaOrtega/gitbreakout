﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class record : MonoBehaviour
{
    private void Start()
    {
        if (!PlayerPrefs.GetString("1Record_Player").Equals(""))
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = PlayerPrefs.GetString("1Record_Player") + " - " + PlayerPrefs.GetInt("1Record_Score");
        }
        else
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Scr - 0";
        }
    }
}
