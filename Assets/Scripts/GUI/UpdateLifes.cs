﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLifes : MonoBehaviour
{
    public bool updateLifes(int n)
    {
        RectTransform[] hijos;
        hijos = this.transform.GetComponentsInChildren<RectTransform>();

        for (int num = 1; num < hijos.Length; num++)
        {
            if(num > n)
            {
                hijos[num].gameObject.SetActive(false);
            }
        }
        if (!hijos[1].gameObject.activeSelf)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
