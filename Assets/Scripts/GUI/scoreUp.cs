﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scoreUp : MonoBehaviour
{
    [HideInInspector]public int score;

    private void Start()
    {
        this.GetComponent<TMPro.TextMeshProUGUI>().text = PlayerPrefs.GetString("Player_Name", "Ply") + " - 0";
    }

    public void scoreChange()
    {
        /*
         * Metodo simple que cambia la puntuacion.
         */
        string text = PlayerPrefs.GetString("Player_Name", "Ply") + " - " + score.ToString();

        this.GetComponent<TMPro.TextMeshProUGUI>().text = text;

        /*
         * Si la puntuacion es mayor que la del record que se vaya sumando.
         */
        if(score > PlayerPrefs.GetInt("1Record_Score"))
        {
            GameObject.Find("GUI/Canvas/MenuInGame/menu/Record").GetComponent<TMPro.TextMeshProUGUI>().text = text;
            PlayerPrefs.SetInt("1Record_Score", score);
            PlayerPrefs.SetString( "1Record_Player", PlayerPrefs.GetString("Player_Name", "Ply") );
        }
        else
        {
            string Rtext = PlayerPrefs.GetString("1Record_Player", "Ply") + " - " + PlayerPrefs.GetInt("1Record_Score").ToString();
            GameObject.Find("GUI/Canvas/MenuInGame/menu/Record").GetComponent<TMPro.TextMeshProUGUI>().text = Rtext;
        }
    }
}
