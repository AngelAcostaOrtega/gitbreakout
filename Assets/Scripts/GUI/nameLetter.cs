﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nameLetter : MonoBehaviour
{
    [SerializeField]
    private GameObject Padre = null;

    public void changeUp()
    {
        Padre.TryGetComponent(out letterVars l);
        l.letterIndx++;
        if (l.letterIndx > 90) l.letterIndx = 65;
        l.letter = char.ConvertFromUtf32(l.letterIndx).ToCharArray()[0];

        Padre.TryGetComponent(out TMPro.TextMeshProUGUI n);
        n.text = l.letter.ToString();
    }
    public void changeDown()
    {
        Padre.TryGetComponent(out letterVars l);
        l.letterIndx--;
        if (l.letterIndx < 65) l.letterIndx = 90;
        l.letter = char.ConvertFromUtf32(l.letterIndx).ToCharArray()[0];

        Padre.TryGetComponent(out TMPro.TextMeshProUGUI n);
        n.text = l.letter.ToString();
    }
}
