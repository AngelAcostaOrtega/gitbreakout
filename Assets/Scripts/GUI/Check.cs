﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class Check : MonoBehaviour
{
    public Sprite Ia, Ib;
    private void Start()
    {
        check();
    }
    public void check()
    {
        Image Im = this.transform.gameObject.GetComponent<Image>();

        Im.sprite = ( PlayerPrefs.GetInt(this.name) == 0 ? Ib : Ia );
    }
}
