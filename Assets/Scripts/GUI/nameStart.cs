﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nameStart : MonoBehaviour
{
    public GameObject letra1 = null, letra2 = null, letra3 = null;

    public void saveName()
    {
        letra1.TryGetComponent(out TMPro.TextMeshProUGUI a);
        letra2.TryGetComponent(out TMPro.TextMeshProUGUI b);
        letra3.TryGetComponent(out TMPro.TextMeshProUGUI c);

        string name = a.text + b.text + c.text;

        PlayerPrefs.SetString("Player_Name", name);
    }
}
