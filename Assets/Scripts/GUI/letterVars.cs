﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class letterVars : MonoBehaviour
{
    [HideInInspector]
    public int letterIndx = 65;
    [HideInInspector]
    public char letter = 'A';
}
