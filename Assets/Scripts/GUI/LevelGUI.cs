﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGUI : MonoBehaviour
{
    public void changeLevel(int lvl)
    {
        GameObject.Find("GUI/Canvas/Level/Panel/levelLbl").GetComponent<TMPro.TextMeshProUGUI>().text = "Nivel " + lvl;
    }
}
